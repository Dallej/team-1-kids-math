/*Author kalle jämsä*/
$(document).ready(function () {

    //tehtävän globaali muuttujat pisteet ja tehtävien määrät
    let pisteet = 0;
    let tehtmaara = 0;

    let plussa = "+";

    //pelaamaan napin functio
    $("#play").click(function () {

        let name = $("#nimi").val();
        $("#ohjeet").hide();
        $("#nimi").hide();
        $("#givename").hide();

        $("#tehtava").removeClass("invisible");
        $("#tehtavanapit").removeClass("invisible");
        $("#pelaaja").html(name);



        //aloita / seuravaa nappi
        $("#seuraava").click(function () {
            $("#oikein").html("");
            $("#väärin").html("");
            $("#testi").html("");
            $("#vastaus").val("");

            $("#plus").html(" + ")

            //tehtävien määrän tulostus
            $("#seuraava").html("Seuraava");
            tehtmaara++;
            $("#tehtmaara").html(tehtmaara);
            $("#kerrat").removeClass("invisible");

            $("#seuraava").prop("disabled", true);

            let level = Number($("input[name=diff]:checked").val());

            //levelin valitseminen
            if (level === 1) {
                let num1 = Number(getRndInteger(1, 10));
                $("#ekanumero").html(num1);

                let num2 = Number(getRndInteger(1, 10));
                $("#tokanumero").html(num2);


                let tulos = num1 + num2;
            //tuloksen tarkistaminen ja vertaaminen
                $("#tarkista").click(function () {
                    let vastaus = Number($("#vastaus").val());
                    $("#seuraava").prop("disabled", false);

                    $("#testi").html(vastaus);
                    if (vastaus == tulos) {
                        $("#oikein").html("OIKEIN! Voit siirtyä seuraavaan.");
                        $("#seuraava").removeClass("disabled");
                        $("#väärin").html("");
                        pisteet++;
                    } else {
                        $("#väärin").html("Valitettavasti vastaus oli väärin :(");
                        $("#oikein").html("");
                    }


                });


            }
            //levelin valitseminen
            if (level === 2) {
                let num1 = Number(getRndInteger(10, 100));
                $("#ekanumero").html(num1);

                let num2 = Number(getRndInteger(10, 100));
                $("#tokanumero").html(num2);


                let tulos = num1 + num2;

            //tuloksen tarkistaminen ja vertaaminen
                $("#tarkista").click(function () {
                    let vastaus = Number($("#vastaus").val());
                    $("#testi").html(vastaus);
                    if (vastaus == tulos) {
                        $("#oikein").html("OIKEIN! Voit siirtyä seuraavaan.");
                        $("#seuraava").removeClass("disabled");
                        $("#väärin").html("");
                        pisteet++;
                    } else {
                        $("#väärin").html("Valitettavasti vastaus oli väärin :(");
                        $("#oikein").html("");
                    }
                });

            }
            //levelin valitseminen
            if (level === 3) {
                let num1 = getRndInteger(100, 100);
                $("#ekanumero").html(num1);

                let num2 = getRndInteger(100, 1000)
                $("#tokanumero").html(num2);


                let tulos = num1 + num2;
                
                //tuloksen tarkistaminen ja vertaaminen
                $("#tarkista").click(function () {
                    let vastaus = Number($("#vastaus").val());
                    $("#testi").html(vastaus);
                    if (vastaus == tulos) {
                        $("#oikein").html("OIKEIN! Voit siirtyä seuraavaan.");
                        $("#seuraava").removeClass("disabled");
                        $("#väärin").html("");
                        pisteet++;
                    } else {
                        $("#väärin").html("Valitettavasti vastaus oli väärin :(");
                        $("#oikein").html("");
                    }
                });

            }
            //kun tehtäviä on 5 niin modal tulee esiin ja näyttää pisteet ja onnittelee
            if(tehtmaara === 6){
                $("#seuraava").html("Lopeta ja näytä pisteet")
                $("#seuraava").prop("disabled", false);
                $("#modal").modal("show");

                $("#pisteet").html(pisteet);
                $("#pelaaja").html(name);
                $("#saada").removeClass("invisible");
                
            }
            //modalin mitallin määrittelyt
            if(pisteet>=5){
                $("#kulta").show(); 
                $("#hopea").hide(); 
                $("#pronssi").hide();  
                
                pisteet-1;   
            }
            if(pisteet>=3){
                $("#hopea").show();  
                $("#pronssi").hide(); 
                $("#kulta").hide();  
                

            }
            if(pisteet>=1){
                $("#pronssi").show();  
                $("#hopea").hide(); 
                $("#kulta").hide(); 
               
                 

            }else{
                $("#mitali").html("Valitettavasti et saanut yhtään pistettä :("); 
                $("#saada").addClass("invisible");
                $("#hopea").hide(); 
                $("#kulta").hide(); 
                $("#pronssi").hide();  

            }



        });

    });






    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

});