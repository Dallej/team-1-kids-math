$(document).ready(function () {
    let oikeat_vastaukset = [];



    $(".answer").click(function () {
        // lue value-attribuutin arvo muuttujaan, numeerisena
        let vastaus = Number($(this).val());

        let kysymys = $(this).attr("name");

        // $([name=xxx])
        $("[name=" + kysymys + "]").prop("disabled", true);

        // $("#xxx_explanation")
        $("#" + kysymys + "_explanation").show();


        if (vastaus === 1) {
            // muutetaan taustaväirä parent-elementin avulla
            $(this).parent().addClass("oikein");
            // lihavoidaan seuraavan elementin teksti
            $(this).next().addClass("vastaus_teksti");
            // lasketaan oikeat vastaukset
            oikeat_vastaukset.push("oikein");
        } else {
            $(this).parent().addClass("vaarin");
            // haetaan oikea vasutaus, lihavoidaan seuraavan elementin teksti
            $("[name=" + kysymys + "][value=1]").next().addClass("oikea_vastaus");
        }
    });


    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    let kerrat = 0;

    let kysymykset = ["#kysymys1", "#kysymys2", "#kysymys3", "#kysymys4", "#kysymys5"];

    $(".seuraava_q").click(function () {
        if (kysymykset.length > 0) {
            let kysymys = getRndInteger(0, kysymykset.length - 1);
            let esille = kysymykset[kysymys];
            $(esille).show();
            kysymykset.splice(kysymys, 1);
            // painikkeen vanhempi piilotetaan
            $(this).parent().hide();
            kerrat++;

        } else {
            // tähän määrittelyjä mitä tehdään kun kysymykset loppuu
            // LAITA ALEMPAAN OMIEN KYSYMYSTEN MÄÄRÄ>>>                                     tähän
            $("#tulokset").html("Onneksi olkoon sait " + oikeat_vastaukset.length + " / " + " 5    oikein");
            if (oikeat_vastaukset.length === 5) {
                $('#pics').html('<img id="theImg" src="img/kulta.png" />');
                return;
            }
            if (oikeat_vastaukset.length >= 3) {
                $('#pics').html('<img id="theImg" src="img/hopea.png" />');
                return;
            }
            if (oikeat_vastaukset.length >= 1) {
                $('#pics').html('<img id="theImg" src="img/pronssi.png" />');
                return;
            }
        }


    });

});