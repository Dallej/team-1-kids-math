/*    
 *  Author: Lauri Köykkä
 */

$(document).ready(function(){
    /*
     * Matematiikkatehtävän javascript määrittelyt 
     */
    
    /*
     * Mahdollistaa tehtävien määrän ja vaikeustason valinnan.
     */
    $("#tehtava_maara_buttons").children().click(function(){
        $("#tehtava_maara").html($(this).html());
    });
    $("#vaikeustaso_buttons").children().click(function(){
        $("#vaikeustaso").html($(this).html());
    });
     
     /*
      * Global array määrittelyjä joita käytetään tehtävien tarkistuksessa ja
      * printtauksessa, tehtävien välillä liikkumisessa sekä merkkien printtaamisessa.    
      */
    let ekat = [];
    let tokat = [];
    let tulot = [];
    
    
    
    let painallukset = [];        
    
    let merkki = [
        '<i class="fa fa-5x fa-check harmaa" aria-hidden="true"></i>',
        '<i class="fa fa-5x fa-check vihrea"  aria-hidden="true"></i>',        
        '<i class="fa fa-5x fa-times punainen"  aria-hidden="true"></i>'
    ];    
    let merkit = [];
    
    /*
     *  Satunnaislukufunktio
     */
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    
    /*
     * Funktio arpoo numerot joita käytetään jakolaskuun ja lisää ne arrayhin,
     * sekä tulostaa sivulle harmaat merkit.     
     */
    
    function doMath() {
        let ylaraja = $("#tehtava_maara").html();
        let taso_ala = 0;
        let taso_yla = 0;
        if ($("#vaikeustaso").html() === "Helppo") {
            taso_ala = 1;
            taso_yla = 5;
        } else if ($("#vaikeustaso").html() === "Normaali") {
            taso_ala = 3;
            taso_yla = 9;
        }
        for (let i = 0; i < ylaraja; i++) {
            let eka = getRndInteger(taso_ala, taso_yla);
            ekat.push(eka);
            let toka = getRndInteger(taso_ala, taso_yla);
            tokat.push(toka);
            let tulo = eka * toka;
            tulot.push(tulo);
            
            merkit.push(merkki[0]);
        }
    }
    
    /*
     * Tehtavan aloitus painikkeen toiminnan määrittelyt. Esittää modaalin jos
     * tehtävien määrää tai tasoa ei ole valittu. 
     * 
     * Tyhjentää arvot tehtävästä, ottaa esille ja aloittaa sen.
     */      
    $("#tehtava_aloitus").click(function(){
        if (($("#tehtava_maara").html() === "") | $("#vaikeustaso").html() === "") {
            $("#error").modal("show");
            return;
        }
        
        ekat = [];
        tokat = [];
        tulot = [];
        painallukset = [];
        merkit = [];
  
        doMath();
        $("#tehtava").html(tulot[0] + " / " + ekat[0] + " = ");
        
        $("#menestyminen").html(merkit);
        $("#tehtava_nro").html(painallukset.length + 1);
        
        $("#p1").removeClass("piilotettu");
        $("#p2").removeClass("piilotettu");
        $("#p3").removeClass("piilotettu");
        $("#menestyminen").removeClass("piilotettu");
    });
    
    
    /*
     * Painike "seuraava", ottaa esiin seuraavan tehtävän ja lisää arrayhin pituutta
     * jotta "edellinen" painikkeella voi myös liikkua tehtävissä taaksepäin.     
     */
    $("#seuraava").click(function(){
        painallukset.push("uusi");
        if (painallukset.length >= ekat.length) {           
            $("#seuraava").addClass("disabled");
            painallukset.splice(0, 1);
            return;
        } else {
            $("#seuraava").removeClass("disabled");
        }               
        
        let kerta = painallukset.length;
        let eka_s = ekat[kerta];
        let tulo_s = tulot[kerta];
        $("#tehtava").html(tulo_s + " / " + eka_s + " = ");
        
        $("#vastaus").val("0");
        $("#tehtava_nro").html(painallukset.length+1);
    });
    
    
    /*
     * Painike "edellinen", ottaa esiin edellisen tehtävän perustuen siihen
     * montako kertaa "seuraava" -painiketta on painettu
     */
    $("#edellinen").click(function(){
        painallukset.splice(0, 1);
        let kerta = painallukset.length;
        
        let eka_e = ekat[kerta];
        let tulo_e = tulot[kerta];
        $("#tehtava").html(tulo_e + " / " + eka_e + " = ");
        $("#tehtava_nro").html(painallukset.length+1);
    });
    
        
    /*
     * Tarkista painikkeen toiminnan määrittelyt. Lisää oikein merkin jos vastaus
     * oli oikein ja väärin merkin jos vastaus oli väärin.
     */    
    $("#tarkista").click(function(){
        let kerta = painallukset.length;
                
        if (Number($("#vastaus").val()) === tokat[kerta]) {
            $("#testi").html($("#vastaus").val());
            merkit.splice(painallukset.length, 1, merkki[1]);
            $("#menestyminen").html(merkit);
        } else {
            merkit.splice(painallukset.length, 1, merkki[2]);
            $("#menestyminen").html(merkit);
        }
        
        
       
        
        /*
         * Tarkistaa montako oikein tai väärin merkkiä arrayssa on, ja jos harmaita
         * ei nää ole, antaa se käyttäjälle tuloksen menestyksestään.         
         */
        let kaikki = merkit.includes('<i class="fa fa-5x fa-check harmaa" aria-hidden="true"></i>');
        if (kaikki === false) {
            var maara = 0;
            for (var i = 0; i < merkit.length; i++) {
                if (merkit[i] === '<i class="fa fa-5x fa-check vihrea"  aria-hidden="true"></i>'){
                    maara++;
                }
            }
            
            if (maara < merkit.length * 0.2) {
                $("#palkinto").html("Onneksi olkoon sait " + maara + " / " + tokat.length
                        + " oikein. Jakolaskutaidoissasi on vielä parantamista. \n\
                            Yritä tehtävää uudelleen kerrattuasi kertolaskuja.");
            } else if (maara < merkit.length * 0.5) {
                $("#palkinto").html("Onneksi olkoon sait " + maara + " / " + tokat.length
                        + " oikein. Onnistuit ihan kohtalaisesti tehtävässä.");
                $("#kuva").html('<img src="img/pronssi.png" alt=""/>');
            } else if (maara < merkit.length * 0.8) {
                $("#palkinto").html("Onneksi olkoon sait " + maara + " / " + tokat.length
                        + " oikein. Onnistuit hyvin jakolaskutehtävässä!. Yritä\n\
                            vielä uudelleen jotta pääsisit vieläkin parempaan tulokseen!");
                $("#kuva").html('<img src="img/hopea.png" alt=""/>');
            } else if (maara < merkit.length) {
                $("#palkinto").html("Onneksi olkoon sait " + maara + " / " + tokat.length
                        + " oikein. Onnistuit mahtavasti tehtävässä! Vain parista pisteestä\n\
                            kiinni niin olisit saanut täydet pisteet!");
                $("#kuva").html('<img src="img/kulta.png" alt=""/>');
            } else {
                $("#palkinto").html("Onneksi olkoon sait " + maara + " / " + tokat.length
                        + " oikein. Sait täydet pisteet!. Osaat mainiosti jakolasku\n\
                            tehtäviä!");
                $("#kuva").html('<img src="img/kulta.png" alt=""/>');
            } 
            $("#completion").modal("show");
            
        }
    });
                


    /*
     * kyselyn javasript määrittelyt (tunnilla tehty pohja).
     */


    let oikeat_vastaukset = [];
    
    $(".answer").click(function () {
        // lue value-attribuutin arvo muuttujaan, numeerisena
        let vastaus = Number($(this).val());      
        
        let kysymys = $(this).attr("name");
        
        // $([name=xxx])
        $("[name=" + kysymys + "]").prop("disabled", true);
        
        // $("#xxx_explanation")
        $("#" + kysymys + "_explanation").show();


        if (vastaus === 1) {
            // muutetaan taustaväirä parent-elementin avulla
            $(this).parent().addClass("oikein");
            // lihavoidaan seuraavan elementin teksti
            $(this).next().addClass("vastaus_teksti");
            // lasketaan oikeat vastaukset
            oikeat_vastaukset.push("oikein");
        } else {
            $(this).parent().addClass("vaarin");
            // haetaan oikea vasutaus, lihavoidaan seuraavan elementin teksti
            $("[name=" + kysymys + "][value=1]").next().addClass("oikea_vastaus");
        }        
    });
    
    
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    
    
    
    let kysymykset = ["#kysymys2", "#kysymys3", "#kysymys4", "#kysymys5", "#kysymys6", "#kysymys7"];

    $(".seuraava_q").click(function () {
        if (kysymykset.length > 0) {
            let kysymys = getRndInteger(0, kysymykset.length-1);
            let esille = kysymykset[kysymys];
            $(esille).show();
            kysymykset.splice(kysymys, 1);
            // painikkeen vanhempi piilotetaan
            $(this).parent().hide();
        } else {
            // Tehtävien loputtua näytetään käyttäjän onnistuminen ja palkitaan 
            // sen mukaan
            $("#onnea").modal("show");            
            if (oikeat_vastaukset.length < 4) {                
                $("#tulokset").html("Onneksi olkoon sait " + oikeat_vastaukset.length 
                        + " / " + " 6    oikein" + "<br>" + "Yritä visaa uudelleen\n\
                        ja pyri saamaan ensi kerralla parempi tulos!");
                $("#visakuva").html('<img src="img/pronssi.png" alt=""/>');
            } else if (oikeat_vastaukset.length < 6) {                
                $("#tulokset").html("Onneksi olkoon sait " + oikeat_vastaukset.length 
                        + " / " + " 6    oikein" + "<br>" + "Onnistuit visassa\n\
                        erittäin hyvin! Ei ole paljosta kiinni että pääsisit \n\
                        täysiin pisteisiin!");
                $("#visakuva").html('<img src="img/hopea.png" alt=""/>');
            } else {
                $("#tulokset").html("Onneksi olkoon sait " + oikeat_vastaukset.length 
                        + " / " + " 6    oikein" + "<br>" + "Onneksi olkoon\n\
                        tietämyksesi aiheesta on erittäin hyvä ja ansaitset\n\
                        parhaimmat pisteet tästä visasta! Testaa onko tietämyksesi\n\
                        yhtä hyvä muissakin visoissa!");
                $("#visakuva").html('<img src="img/kulta.png" alt=""/>');
            }
            
        }
    });

    // Painike joka lataa sivun uudelleen
    
    $("#reset").click(function(){
        location.reload();
    });
});
