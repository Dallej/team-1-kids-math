//author: Juhani Moilanen

$(document).ready(function () {
    let kerrat = 0;
    let pisteet = 0;
    // play -painikkeen funktiot
    $("#play").click(function () {
        let name = $("#name").val();
        $("#name").hide();
        $("#play").hide();
        $("#answer").removeClass("invisible");
        $("#check").removeClass("invisible");
        $("#next").removeClass("invisible");
        $("#yourname").html(name);
        
    });
    // rng 
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    // next -painike, laskee tehtävien määrät, siirtyy seuraavaan tehtävään
    $("#next").click(function () {
        $("#next").html('Seuraava');
        $("#counter").removeClass("invisible");
        $("#kerrat").html(pisteet);
        kerrat++;
        $("#next").prop("disabled", true);
        $("#oikein").html("");
        $("#vaarin").html("");
        let difficulty = Number($("input[name=difficulty]:checked").val());
        // vaikeustason valinta
        if (difficulty === 1) {
            let number1 = (getRndInteger(2, 5));
            let number2 = (getRndInteger(2, 5));
            $("#number1").html(number1);
            $("#number2").html(number2);
            $("#merkki").html(" * ");
            $("#merkki2").html(" =");

            $("#check").click(function () {
                $("#next").prop("disabled", false);
                let vastaus = Number($("#answer").val());
            if(number1 * number2 === vastaus){
                pisteet++;
                $("#vaarin").html("");
                $("#oikein").html("Vastaus on oikein.");
                
                return;
                }
                else{
                    $("#oikein").html("");
                    $("#vaarin").html("Vastaus on väärin.");
                    
                    
                }
            });
        }
    
        if (difficulty === 2) {
            let number1 = (getRndInteger(6, 10));
            let number2 = (getRndInteger(6, 10));
            $("#number1").html(number1);
            $("#number2").html(number2);
            $("#merkki").html(" * ");
            $("#merkki2").html(" =");

            $("#check").click(function () {
                $("#next").prop("disabled", false);
                let vastaus = Number($("#answer").val());
            if(number1 * number2 === vastaus){
                pisteet++;
                $("#vaarin").html("");
                $("#oikein").html("Vastaus on oikein.");
                
                return;
                }
                else{
                    $("#oikein").html("");
                    $("#vaarin").html("Vastaus on väärin.");
                    
                    
                }
            });
        }
        if (difficulty === 3) {
            let number1 = (getRndInteger(11, 20));
            let number2 = (getRndInteger(11, 20));
            $("#number1").html(number1);
            $("#number2").html(number2);
            $("#merkki").html(" * ");
            $("#merkki2").html(" =");

            $("#check").click(function () {
                $("#next").prop("disabled", false);
                
                // vastaus oikein/väärin
                let vastaus = Number($("#answer").val());
                if(number1 * number2 === vastaus){
                    pisteet++;
                    $("#vaarin").html("");
                    $("#oikein").html("Vastaus on oikein.");
                    return;
                    }
                    else{
                        $("#oikein").html("");
                        $("#vaarin").html("Vastaus on väärin.");
                        
                    }
                });
        }
        $("#answer").val("");
        //pistemäärän tulostus
        if(kerrat === 6){

            $("#next").html("Pisteet");
            $("#pisteet").modal("show");
            $("#score").html(pisteet);
            
            
        }
        //palkintojen tulostus
    if (pisteet>=5){
        $("#kulta").show();
        $("#pronssi").hide();
        $("#hopea").hide();
        pisteet - 1;
        return;
    }
    if (pisteet>=3){
        $("#hopea").show();
        $("#kulta").hide();
        $("#pronssi").hide();
        return;
    }
    if(pisteet>=1){
        $("#pronssi").show();
        $("#kulta").hide();
        $("#hopea").hide();
    }
    else{
        $("#pronssi").hide();
        $("#kulta").hide();
        $("#hopea").hide();
    }
    

    });
    
});

    


    